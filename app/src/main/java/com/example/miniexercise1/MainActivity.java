package com.example.miniexercise1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void openScreenOne(View view) {
        Intent intent=new Intent(this,ScreenOneActivity.class);
        startActivity(intent);
    }

    public void openScreenTwo(View view) {
        Intent intent=new Intent(this,ScreenTwoActivity.class);
        startActivity(intent);
    }

    public void openScreenThree(View view) {
        Intent intent=new Intent(this,ScreenThreeActivity.class);
        startActivity(intent);
    }
}
